package com.greatproject.dishonline.loader;

import com.greatproject.dishonline.entity.Tree;
import com.greatproject.dishonline.repository.TreeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class TreeLoader {

    @Autowired
    TreeRepository treeRepository;

    @EventListener
    public void appReady(ApplicationReadyEvent event) {

       // Tree tree=treeRepository.findById(1L);


        if(treeRepository.findAll().isEmpty()) {

            treeRepository.save(new Tree("rootElement", null));
            treeRepository.save(new Tree("firstElement", 1L));
            treeRepository.save(new Tree("secondElement", 2L));
        }


    }
}
