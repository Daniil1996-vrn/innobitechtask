package com.greatproject.dishonline.repository;

import com.greatproject.dishonline.entity.Tree;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TreeRepository extends JpaRepository<Tree,Long> {

   // Tree findById(Long id);

}
