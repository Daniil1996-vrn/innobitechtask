package com.greatproject.dishonline.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.greatproject.dishonline.entity.Tree;
import com.greatproject.dishonline.repository.TreeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class TreeController {
    @Autowired
    TreeRepository treeRepository;

    @GetMapping(path="/showAll",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Tree>  showAllMethod() {

        List<Tree> treeArrayList=new ArrayList<>();

        treeArrayList=treeRepository.findAll();
        System.out.println("treeArrayList:"+treeArrayList.toString());
        return  treeArrayList;

    }

    @PostMapping(value = "/createElement",
            consumes = MediaType.APPLICATION_JSON_VALUE,

            produces = MediaType.APPLICATION_JSON_VALUE
    )

    public  @ResponseBody
    List<Tree> mainPageWorksMethod(
            @RequestBody Tree tree
    ) throws JsonProcessingException {


        Tree forInsertElementTree=new Tree();

        System.out.println("ParentId:"+tree.getParentId());

        forInsertElementTree.setElement(tree.getElement());
        forInsertElementTree.setParentId(tree.getParentId());
        treeRepository.save(forInsertElementTree);


        return  treeRepository.findAll();
    }



}
