package com.greatproject.dishonline;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class DishOnlineApplication {

	public static void main(String[] args) {


		SpringApplication.run(DishOnlineApplication.class, args);
	}


}


