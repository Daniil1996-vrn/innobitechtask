import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HeaderComponent} from './header/header.component';


import {FooterComponent} from './footer/footer';
import { HomeComponent } from './home/home.component';



import {HttpClient, HttpClientModule} from '@angular/common/http';


import { SafeMapPipe } from './safe-map.pipe';
import {FormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSliderModule} from '@angular/material/slider';
import {MatRadioModule} from '@angular/material/radio';
import {MatCheckboxModule} from '@angular/material/checkbox';

import {MatInputModule} from '@angular/material/input';
import {MatButtonModule, MatNativeDateModule, MatPaginatorModule, MatSortModule, MatTableModule} from '@angular/material';
import {_MatMenuDirectivesModule, MatMenuModule} from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSelectModule} from '@angular/material/select';
import {MatSnackBarModule} from '@angular/material/snack-bar';

import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';




import { registerLocaleData} from '@angular/common';
import localeRu from '@angular/common/locales/fr';

import {MatDialogModule} from '@angular/material/dialog';





import {NouisliderModule} from 'ng2-nouislider';

import {MatExpansionModule} from '@angular/material';

registerLocaleData(localeRu, 'ru');

const appRoutes: Routes = [
  {path: '', component: HomeComponent}

];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,


    FooterComponent,
    HomeComponent,

    SafeMapPipe

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatRadioModule,
    MatCheckboxModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatSnackBarModule,
    FontAwesomeModule,
    MatTableModule,
    MatSortModule,
    MatButtonModule,
    MatPaginatorModule,
    MatDialogModule,
    _MatMenuDirectivesModule,
    MatMenuModule,
    NouisliderModule,
    MatExpansionModule
  ],
  providers: [
    MatDatepickerModule],
  bootstrap: [AppComponent],
  entryComponents: [
  ]
})
export class AppModule { }
