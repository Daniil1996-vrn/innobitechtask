import { Component, OnInit } from '@angular/core';

import {HttpClient} from '@angular/common/http';

import {environment} from "../../environments/environment";
import {Router} from '@angular/router';

import {Tree} from "../classes/Tree";

import { MatPaginator, MatTableModule, MatTableDataSource } from '@angular/material';

import {MatExpansionModule} from '@angular/material/expansion';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private http: HttpClient, private r: Router) {}

apiUrl = environment.apiUrl;
  baseUrl = this.apiUrl;

treeArray: Tree[] = [];

tree:Tree=new Tree();

treeObject:Tree=new Tree();

isChecked;

childTreeArray: Tree[] = [];

childTreeArray2: Tree[] = [];

isVisible;

  ngOnInit() {

 this.isChecked=0;

 this.isVisible=0;

this.http.get(this.baseUrl + '/showAll').subscribe((data: Tree[]) =>
      {
      this.treeArray=data;
      },
          error => console.log(error));
        console.log(this.treeArray);


}

 selectChangeHandler (value) {
          //update the ui

          var vParentId=this.treeArray.find(x=>x.element == value).id;
          console.log("vParentId:"+vParentId);

          this.tree.parentId = Number(vParentId);
           //console.log(" Value is : ", this.addingWork.category);
        }

        onSubmit()
        {
        if(this.isChecked==1) this.tree.parentId=null;
        this.http.post(this.baseUrl + '/createElement',this.tree).subscribe((data: Tree[]) =>
              {
              this.treeArray=data;
              },
                  error => console.log(error));
                console.log(this.treeArray);


        }

onSelect(event) {
     if ( event.target.checked ) {
        this.isChecked=1;
    }

    if ( !event.target.checked ) {
            this.isChecked=0;
        }

}

//*ngIf="item[k]?.id==item[k+1]?.parentId"

/*

<div *ngFor="let item of treeArray;let k = index ">

  <p>
    <a class="btn btn-primary" data-toggle="collapse" href="#{{item[k]?.id}}" role="button" aria-expanded="false" aria-controls="collapseExample">
      {{item[k]?.element}}
    </a>

  </p>
  <div  class="collapse" id="{{item[k]?.id}}">
    <div  class="card card-body">
      {{item[k]?.element}}
    </div>
  </div>


</div>


*/

/*list = treeArray;
  selectedList: List;
  onSelect(list: Tree[]): void {
    list.hide = !list.hide;
    this.selectedList = list;
  }*/


  //https://stackblitz.com/edit/nested-accordion-vpehhr?file=app%2Fapp.component.html

  getChildsElementOfTree(id)
  {

 // if(this.isVisible==0)
   this.isVisible=1;

  //if(this.isVisible==1) this.isVisible=0;


  this.childTreeArray.length=0;
  this.childTreeArray = this.treeArray.filter(
            element => element.parentId ===id );

            console.log(this.childTreeArray);

          /*  for(let i=0;i<this.treeArray.length-1;i++)
            {
            if(this.treeArray[i].parentId==id)  this.treeObject=this.treeArray[i];
            }*/



           /* this.childTreeArray=this.treeArray;
            return this.childTreeArray;*/


  }

  getChildsElementOfTree2()
    {
    /*this.childTreeArray = this.treeArray.filter(
              element => element.id === parentId);*/

            /*  for(let i=0;i<this.treeArray.length-1;i++)
              {
              if(this.treeArray[i].parentId==id)  this.treeObject=this.treeArray[i];
              }*/

              this.childTreeArray2=this.treeArray;
              return this.childTreeArray2;

    }

}
